#!/bin/bash -e

apt-get update
apt-get install -y python3-pip zip
pip3 install awscli
mkdir ~/.aws
echo "[default]
aws_access_key_id = ${KEY_ID}
aws_secret_access_key = ${ACCESS_KEY}
" > ~/.aws/credentials
echo "[default]
output = json
" > ~/.aws/config