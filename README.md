SRE Exam
---------
Build and deploy an Application.

Output
---------
Navigate to exam.marklouisdelapena.com

**Within a readme or similar, articulate the reasons for the decisions you made**

I chose terraform as my Infrastructure-as-a-code since it is easier to understand and it is not too complex also, the codes/syntax are documented and readily available when you search for it. 

As for AWS like Elastic Beanstalk, i chose this platform because you can quickly deploy and manage applications in the AWS Cloud without having to learn exetensively about the infrastructure that runs those applications. I also used AWS Route 53 since it is also simple and it is more presentable rather than with just an IP address.

I also chose gitlab as my repository because of its continous integration and it is free and no external CI tools is required.

Repository
---------
https://gitlab.com/zeta-sre-exam

Built With
---------
Terraform,
AWS Elastic Beanstalk,
AWS Route 53,
AWS S3,
Docker,
Gitlab CI/CD

Author
---------
Mark Louis De la Peña
