FROM ruby:2.6

WORKDIR /app
COPY . /app
RUN gem install bundler:1.17.2
RUN bundle install

EXPOSE 80

CMD ["bundle", "exec", "rackup", "--host", "0.0.0.0", "-p", "80"]
